#!/usr/bin/env python2.7
from __future__ import print_function

from ec2_metadata import ec2_metadata
from pprint import pprint

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import boto3
import sys

def err(*args):
    print(*args, file=sys.stderr)

ec2 = boto3.client('ec2', region_name=ec2_metadata.region)

dumb = ec2.describe_tags(Filters=[{'Name': 'resource-id', 'Values': [ec2_metadata.instance_id]}])

tags = {x['Key']: x['Value'] for x in dumb['Tags']}

with open('/opt/vpc-mesh/share/tag_spec.yml', 'rb') as f:
    tag_spec = load(f, Loader=Loader)

config = {}

for k,v in tag_spec.iteritems():
    val = tags.get(k, v['default'])
    if val not in v['values']:
        err('{0}: {1} is invalid. Acceptable values: {2}. Using default {3} instead.'.format(k, val, v['values'], v['default']))
        val = v['default']
    config[k] = val

with open('/var/lib/vpcm/tag_config.yml', 'wb') as f:
    dump(config, f, Dumper=Dumper, default_flow_style=False, explicit_start=True)
