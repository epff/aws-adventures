#!/usr/bin/env bash

root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$root"

if [ ! -d artifacts ]; then
    mkdir artifacts
fi

# standalone scripts
for i in vpcm_manage_routes vpcm_s3_state_updater vpcm_scan_regions vpcm_scan_vpcs; do
    zip -j -X -9 "artifacts/${i}.zip" "lambda/${i}/${i}.py"
done

# scripts with pip packages / multiple files
for i in vpcm_cfn_helper; do
    (
        cd "lambda/${i}"
        zip -X -9 -r "${root}/artifacts/${i}.zip" ./* -x \*.pyc
    )
done

tar cvzf artifacts/vpc-mesh.tar.gz --exclude '*.sw?' vpc-mesh

cp vpcm_cfn_regional-template.yaml artifacts/
