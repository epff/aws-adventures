from pprint import pformat, pprint

import boto3
import logging
import os

logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.CRITICAL)

ec2 = boto3.resource('ec2')

def does_exact_route_exist(cidr_block, instance, route_table):
    logger.debug('does_route_exist({0}, {1}, {2})'.format(cidr_block, instance, route_table))

    for route in route_table.routes:
        if route.destination_cidr_block == cidr_block and \
                route.instance_id == instance.id and \
                route.network_interface_id == instance.network_interfaces[0].id:
                    logger.debug('found matching route: {0}'.format(route))
                    return True

    logger.debug('no matching routes found')
    return False

def lambda_handler(event, context):
    logger.debug('invoked with event: {0}'.format(event))

    for req in ['action', 'cidr-block', 'instance-id']:
        if req not in event:
            raise Exception('Missing required parameter: {0}.'.format(req))

    if event['action'] not in ['create', 'delete']:
        raise Exception('Action must be create or delete.')

    instance = ec2.Instance(event['instance-id'])

    vpc_tags = {tag['Key']: tag['Value'] for tag in instance.vpc.tags}
    logger.info('tags on {0}: {1}'.format(instance.vpc, vpc_tags))

    # we need to allow for the case where we're deleting routes from a vpc where vpc_mesh:connect
    # has just been set to false (or has been untagged)
    if event['action'] == 'create':
        if vpc_tags.get('vpc_mesh:connect') != 'true':
            raise Exception('{0} is not tagged with "vpc_mesh:connect: true"'.format(vpc))

        if 'vpc_mesh:include_subnets' not in vpc_tags:
            raise Exception('{0} is missing required tag: vpc_mesh:include_subnets'.format(vpc))

        if vpc_tags['vpc_mesh:include_subnets'] not in ['all', 'manual', 'none']:
            raise Exception('{0}: vpc_mesh:include_subnets must be "all", "manual", or "none"'.format(vpc))

        if vpc_tags['vpc_mesh:include_subnets'] == 'none':
            return '{0} is tagged with "vpc_mesh:include_subnets: none". No action taken.'.format(vpc)

        if vpc_tags['vpc_mesh:include_subnets'] == 'manual':
            return '{0} is tagged with "vpc_mesh:include_subnets: manual" and that is not suppported yet. No action taken.'.format(vpc)

    route_desc = '{0} via {1} ({2})'.format(event['cidr-block'], instance.network_interfaces[0].id, instance.id)

    # filter out unused route tables
    route_tables = [x for x in instance.vpc.route_tables.all() if len(x.associations) != 0]

    msg = ''

    for rt in route_tables:
        route_exists = does_exact_route_exist(event['cidr-block'], instance, rt)

        if event['action'] == 'create':
            if route_exists:
                msg = '{0}{1}: route {2} already exists, no action taken.\n'.format(msg, rt.id, route_desc)
            else:
                conflicts = [x for x in rt.routes if x.destination_cidr_block == event['cidr-block']]
                if len(conflicts) > 0:
                    route = conflicts[0]
                    logger.info('found conflicting route: {0}'.format(route))

                    # I'm choosing to ignore a real conflict since create_route will raise a nice exception for me
                    if route.state == 'blackhole':
                        route.replace(DestinationCidrBlock=event['cidr-block'], InstanceId=instance.id)
                        logger.info('replaced blackhole route: {0}'.format(route))
                        msg = '{0}{1}: route {2} replaced existing blackhole route.\n'.format(msg, rt.id, route_desc)
                        continue

                route = rt.create_route(DestinationCidrBlock=event['cidr-block'], InstanceId=instance.id)
                logger.info('created route: {0}'.format(route))
                msg = '{0}{1}: route {2} created.\n'.format(msg, rt.id, route_desc)

        elif event['action'] == 'delete':
            if route_exists == False:
                msg = '{0}{1}: route {2} does not exist, no action taken.\n'.format(msg, rt.id, route_desc)
            else:
                route = ec2.Route(rt.id, event['cidr-block'])
                route.delete()
                logger.info('deleted route: {0}'.format(route))
                msg = '{0}{1}: route {2} deleted.\n'.format(msg, rt.id, route_desc)

    return msg

