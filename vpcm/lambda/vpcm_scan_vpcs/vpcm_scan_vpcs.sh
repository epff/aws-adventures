#!/bin/sh

# sam local is really neat, but this is even faster

eval $(jq ".vpcm_scan_vpcs" sam_env.json | sed -nE '/^  /s/"(.*)": (.*"),?/export \1=\2/gp')

python3.6 vpcm_scan_vpcs.py
