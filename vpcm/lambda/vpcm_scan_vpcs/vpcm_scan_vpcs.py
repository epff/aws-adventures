import ipaddress
import json
import logging
import os

from botocore.exceptions import ClientError

import boto3

logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.CRITICAL)

DRY_RUN = True

try:
    if os.environ['DRY_RUN'].lower() in ('false', '0'):
        DRY_RUN = False
except KeyError:
    pass

INSTANCE_PROFILE = os.environ['INSTANCE_PROFILE']
INSTANCE_TYPE = os.environ['INSTANCE_TYPE']
REGIONAL_BUCKET = os.environ['REGIONAL_BUCKET']
USER_DATA_KEY = os.environ['USER_DATA_KEY']

ec2 = boto3.resource('ec2')
client = boto3.client('ec2')
s3 = boto3.resource('s3')


# TODO: do something smart here
def find_image_id():
    images = {
        'us-east-1': 'ami-6057e21a',
        'us-east-2': 'ami-aa1b34cf',
        'us-west-1': 'ami-1a033c7a',
        'us-west-2': 'ami-32d8124a',
        'ca-central-1': 'ami-ef3b838b',
        'eu-west-1': 'ami-760aaa0f',
        'eu-central-1': 'ami-e28d098d',
        'eu-west-2': 'ami-e3051987',
    }
    return images[os.environ['AWS_REGION']]


# TODO: do something smarter than picking the first keypair we find
def find_key_name():
    for kp in client.describe_key_pairs()['KeyPairs']:
        return kp['KeyName']


def find_security_group(vpc):
    for sg in vpc.security_groups.filter(Filters=[{'Name': 'tag:vpc_mesh:node_security_group', 'Values': ['']}]):
        return sg.group_id

    logger.info('No security group tagged "vpc_mesh:node_security_group" found in {0}.'.format(vpc.id))

    sg = vpc.create_security_group(
        Description='ICMP, SSH, tinc from anywhere; HTTP from IPv6 anywhere', GroupName='VPCM Node')

    sg.create_tags(Tags=[
        {
            'Key': 'vpc_mesh:node_security_group',
            'Value': ''
        },
    ])
    resp = sg.authorize_ingress(IpPermissions=[
        {
            'FromPort': -1,
            'IpProtocol': 'icmp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'ICMP from IPv4 anywhere'
                },
            ],
            'ToPort': -1,
        },
        {
            'FromPort': -1,
            'IpProtocol': '58',
            'Ipv6Ranges': [
                {
                    'CidrIpv6': '::/0',
                    'Description': 'ICMP from IPv6 anywhere'
                },
            ],
            'ToPort': -1,
        },
        {
            'FromPort': 22,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'SSH from IPv4 anywhere'
                },
            ],
            'Ipv6Ranges': [
                {
                    'CidrIpv6': '::/0',
                    'Description': 'SSH from IPv6 anywhere'
                },
            ],
            'ToPort': 22,
        },
        {
            'FromPort': 655,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'tinc from IPv4 anywhere'
                },
            ],
            'Ipv6Ranges': [
                {
                    'CidrIpv6': '::/0',
                    'Description': 'tinc from IPv6 anywhere'
                },
            ],
            'ToPort': 655,
        },
        {
            'FromPort': 655,
            'IpProtocol': 'udp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'tinc from IPv4 anywhere'
                },
            ],
            'Ipv6Ranges': [
                {
                    'CidrIpv6': '::/0',
                    'Description': 'tinc from IPv6 anywhere'
                },
            ],
            'ToPort': 655,
        },
        {
            'FromPort': 80,
            'IpProtocol': 'tcp',
            'Ipv6Ranges': [
                {
                    'CidrIpv6': '::/0',
                    'Description': 'HTTP from IPv6 anywhere'
                },
            ],
            'ToPort': 80,
        },
    ])

    logger.info('Created {0} in {1}.'.format(sg, vpc.id))

    return sg.group_id


def find_transit_subnet(vpc):
    for subnet in vpc.subnets.filter(Filters=[{
            'Name': 'state',
            'Values': ['available']
    }, {
            'Name': 'tag:vpc_mesh:use_for_transit',
            'Values': ['true']
    }]):
        if subnet.assign_ipv6_address_on_creation and subnet.map_public_ip_on_launch and \
                len(subnet.ipv6_cidr_block_association_set) > 0:
            return subnet

    logger.info('No transit subnet found in {0}. Trying to create one.'.format(vpc.id))
    return create_transit_subnet(vpc)


def create_transit_subnet(vpc):
    logger.debug('{0} contains {1} IPv4 blocks: {2}'.format(vpc.id, len(vpc.cidr_block_association_set), \
            ', '.join([x['CidrBlock'] for x in vpc.cidr_block_association_set])))

    if len(vpc.ipv6_cidr_block_association_set) == 0:
        logger.warn('{0} does not have an IPv6 CIDR block association. This is not supported.'.format(vpc.id))
        return None

    logger.debug('{0} contains {1} IPv6 blocks: {2}'.format(vpc.id, len(vpc.ipv6_cidr_block_association_set), \
            ', '.join([x['Ipv6CidrBlock'] for x in vpc.ipv6_cidr_block_association_set])))

    igws = list(vpc.internet_gateways.all())
    if len(igws) == 0:
        logger.warn('{0} does not have an internet gateway. This is required.'.format(vpc.id))
        return False
    igw = igws[0]

    # I'd like to be able to add an additional IPv4 block to the VPC to use for transit,
    # but then I'd need a central registry to ensure it's unique across all connected VPCs
    # in all regions. That's a lot of extra complexity.
    #cidr_block = TBD
    #resp = client.associate_vpc_cidr_block(CidrBlock=cidr_block, VpcId=vpc.id)
    #logger.debug('client.associate_vpc_cidr_block(CidrBlock={0}, VpcId={1}) returned:\n{2}'.format(
    #    cidr_block, vpc.id, json.dumps(resp)))

    # find the last available /28 in the VPC's main IPv4 block
    v4_net = ipaddress.ip_network(vpc.cidr_block)
    v4_blocks = [ipaddress.ip_network(x.cidr_block) for x in vpc.subnets.all()]
    new_ipv4_block = None
    for pb in reversed(list(v4_net.subnets(new_prefix=28))):
        for b in v4_blocks:
            if pb.overlaps(b):
                break
        else:
            new_ipv4_block = pb.with_prefixlen
            break
    else:
        logger.info('There are no free /28 IPv4 blocks in {0}. Cannot create transit subnet.'.format(vpc.id))
        return None

    # find the last available /64 in the VPC's IPv6 block
    v6_net = ipaddress.ip_network(vpc.ipv6_cidr_block_association_set[0]['Ipv6CidrBlock'])
    v6_blocks = [
        ipaddress.ip_network(x.ipv6_cidr_block_association_set[0]['Ipv6CidrBlock'])
        for x in vpc.subnets.all()
        if len(x.ipv6_cidr_block_association_set) == 1
    ]
    new_ipv6_block = None
    for pb in reversed(list(v6_net.subnets(new_prefix=64))):
        for b in v6_blocks:
            if pb.overlaps(b):
                break
        else:
            new_ipv6_block = pb.with_prefixlen
            break
    else:
        logger.info('There are no free /64 IPv6 blocks in {0}. Cannot create transit subnet.'.format(vpc.id))
        return None

    logger.info('Creating transit subnet [{0}, {1}] in {2}.'.format(new_ipv4_block, new_ipv6_block, vpc.id))
    subnet = vpc.create_subnet(CidrBlock=new_ipv4_block, Ipv6CidrBlock=new_ipv6_block)
    logger.debug('Created {0} in {1}.'.format(subnet.id, vpc.id))

    rt = vpc.create_route_table()
    logger.debug('Created {0} in {1}.'.format(rt.id, vpc.id))

    route = rt.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=igw.id)
    logger.debug('Created route 0.0.0.0/0 via {0} in {1}.'.format(igw.id, rt.id))
    route = rt.create_route(DestinationIpv6CidrBlock='::/0', GatewayId=igw.id)
    logger.debug('Created route ::/0 via {0} in {1}.'.format(igw.id, rt.id))

    rt.create_tags(Tags=[{
        'Key': 'Name',
        'Value': 'VPC Mesh Transit Subnet Route Table'
    }, {
        'Key': 'vpc_mesh:auto_created',
        'Value': ''
    }])

    rta = rt.associate_with_subnet(SubnetId=subnet.id)
    logger.debug('Associated {0} with {1} ({2}).'.format(rt.id, subnet.id, rta.id))

    resp = client.modify_subnet_attribute(MapPublicIpOnLaunch={'Value': True}, SubnetId=subnet.id)
    resp = client.modify_subnet_attribute(AssignIpv6AddressOnCreation={'Value': True}, SubnetId=subnet.id)

    subnet.create_tags(Tags=[{
        'Key': 'Name',
        'Value': 'VPC Mesh Transit Subnet'
    }, {
        'Key': 'vpc_mesh:use_for_transit',
        'Value': 'true'
    }, {
        'Key': 'vpc_mesh:auto_created',
        'Value': ''
    }])
    return subnet


def get_user_data():
    obj = s3.Object(REGIONAL_BUCKET, USER_DATA_KEY).get()
    user_data = obj['Body'].read()
    logger.debug('downloaded user_data from s3:\n{0}'.format(user_data.decode()))
    return user_data


def launch_new_node(vpc):
    logger.debug('launch_new_node({0})'.format(vpc.id))

    subnet = find_transit_subnet(vpc)
    if subnet == None:
        logger.info('No transit subnet found in {0}. Skipping.'.format(vpc.id))
        return

    logger.info('Using {0} in {1} for transit.'.format(subnet.id, vpc.id))

    sg_id = find_security_group(vpc)
    logger.debug('Using security group {0}.'.format(sg_id))

    key_name = find_key_name()
    logger.debug('Using keypair named {0}.'.format(key_name))

    image_id = find_image_id()
    logger.debug('Using image {0}.'.format(image_id))

    try:
        resp = subnet.create_instances(
            DryRun=DRY_RUN,
            IamInstanceProfile={'Arn': INSTANCE_PROFILE},
            ImageId=image_id,
            InstanceInitiatedShutdownBehavior='terminate',
            InstanceType=INSTANCE_TYPE,
            Ipv6AddressCount=1,
            KeyName=key_name,
            MaxCount=1,
            MinCount=1,
            SecurityGroupIds=[sg_id],
            TagSpecifications=[{
                'ResourceType': 'instance',
                'Tags': [{
                    'Key': 'Name',
                    'Value': 'VPCM node'
                }, {
                    'Key': 'vpc_mesh:node',
                    'Value': ''
                }, {
                    'Key': 'vpc_mesh:readonly:scope',
                    'Value': 'vpc'
                }]
            }],
            UserData=get_user_data())
    except ClientError as e:
        if DRY_RUN and e.response['Error'].get('Code') == 'DryRunOperation':
            resp = e.response
        else:
            raise
    logger.debug('subnet.create_instances() returned:\n{0}'.format(json.dumps(resp, default=str)))


def process_enabled_vpc(vpc):
    logger.debug('process_enabled_vpc({0})'.format(vpc.id))

    for inst in vpc.instances.filter(Filters=[{'Name': 'tag-key', 'Values': ['vpc_mesh:node']}]):
        logger.debug('found instance tagged with "vpc_mesh:node": {0}'.format(inst))
        return

    launch_new_node(vpc)


def process_disabled_vpc(vpc):
    logger.debug('process_disabled_vpc({0})'.format(vpc.id))

    for inst in vpc.instances.filter(Filters=[{'Name': 'tag-key', 'Values': ['vpc_mesh:node']}]):
        logger.info('Found {0} tagged "vpc_mesh:node" in {1} where "vpc_mesh:connect" == "false". Terminating!'.format(
            inst, vpc))

        try:
            resp = inst.terminate(DryRun=DRY_RUN)
        except ClientError as e:
            if DRY_RUN and e.response['Error'].get('Code') == 'DryRunOperation':
                resp = e.response
            else:
                raise
        logger.debug('instance.terminate() returned:\n{0}'.format(json.dumps(resp)))


def process_unknown_vpc(vpc):
    logger.debug('process_unknown_vpc({0})'.format(vpc.id))


def lambda_handler(event, context):
    logger.info('DRY_RUN: {0}'.format(DRY_RUN))

    # find and process all VPCs with tag vpc_mesh:connect == true
    resp = client.describe_vpcs(Filters=[{'Name': 'tag:vpc_mesh:connect', 'Values': ['true']}])
    logger.debug('client.describe_vpcs(tag: "vpc_mesh:connect": "true") returned:\n{0}'.format(json.dumps(resp)))
    vpcs = [ec2.Vpc(vpc['VpcId']) for vpc in resp['Vpcs']]
    for vpc in vpcs:
        process_enabled_vpc(vpc)

    # find and process all VPCs with tag vpc_mesh:connect == false
    resp = client.describe_vpcs(Filters=[{'Name': 'tag:vpc_mesh:connect', 'Values': ['false']}])
    logger.debug('client.describe_vpcs(tag: "vpc_mesh:connect": "false") returned:\n{0}'.format(json.dumps(resp)))
    vpcs = [ec2.Vpc(vpc['VpcId']) for vpc in resp['Vpcs']]
    for vpc in vpcs:
        process_disabled_vpc(vpc)

    # TODO: find and process all VPCs without tag vpc_mesh:connect
    # I don't think the describe_vpcs filter supports excluding tags


if __name__ == "__main__":
    # make our logs look almost the same as lambda's
    from time import gmtime as _gmtime
    logging.basicConfig(format='[%(levelname)s]\t%(asctime)s.%(msecs)03dZ\t%(message)s\n', datefmt='%Y-%m-%dT%H:%M:%S')
    logging.Formatter.converter = _gmtime
    del _gmtime

    print(lambda_handler(None, None))
