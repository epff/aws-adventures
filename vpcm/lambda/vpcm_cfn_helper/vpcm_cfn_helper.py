import http.client
import json
import logging
import os
import traceback

from urllib.parse import urlparse

import boto3
import botocore

logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.DEBUG)

cfn_client = boto3.client('cloudformation')
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')


def lambda_handler(event, context):
    logger.debug('REQUEST:\n{0}'.format(json.dumps(event)))

    # wrapping everything in a broad try/except because cfn will hang
    # indefinitely if we never upload a response to S3
    try:
        if event['RequestType'] == 'Create':
            on_create(event, context)
        elif event['RequestType'] == 'Delete':
            on_delete(event, context)
        elif event['RequestType'] == 'Update':
            on_update(event, context)

    except Exception as e:
        logger.debug(traceback.format_exc())
        sendResponse(event, context, 'FAILED', {})

    sendResponse(event, context, 'SUCCESS', {})


def on_create(event, context):
    add_s3_notification_lambda(event['ResourceProperties']['ConfigBucket'],
                               event['ResourceProperties']['S3StateUpdater'])
    enable_s3_default_encryption(event['ResourceProperties']['ConfigBucket'])


def on_delete(event, context):
    # delete gets called if the PhysicalId of our placeholder resource changes
    # we want to double check the stack is being deleted before doing anything drastic
    resp = cfn_client.describe_stacks(StackName=event['StackId'])
    status = resp['Stacks'][0]['StackStatus']

    if not status.startswith('DELETE'):
        logger.debug('delete invoked but stack status is {0}'.format(status))
        return

    logger.info('stack is being deleted!')

    # cfn won't delete a bucket with objects in it so the stack will fail to delete
    # if we don't empty the bucket
    bucket_name = event['ResourceProperties']['ConfigBucket']
    logger.info('deleting contents of bucket {0}'.format(bucket_name))
    bucket = s3.Bucket(bucket_name)
    bucket.objects.all().delete()


def on_update(event, context):
    add_s3_notification_lambda(event['ResourceProperties']['ConfigBucket'],
                               event['ResourceProperties']['S3StateUpdater'])
    enable_s3_default_encryption(event['ResourceProperties']['ConfigBucket'])


def add_s3_notification_lambda(bucket, function_arn):
    lambda_config = {
        'Id': 'vpcm_nodes_updated',
        'Events': ['s3:ObjectCreated:*', 's3:ObjectRemoved:*'],
        'Filter': {
            'Key': {
                'FilterRules': [{
                    'Name': 'Prefix',
                    'Value': 'nodes/'
                }]
            }
        },
        'LambdaFunctionArn': function_arn
    }
    bn = s3.BucketNotification(bucket)

    if bn.lambda_function_configurations is not None:
        for n in bn.lambda_function_configurations:
            if n == lambda_config:
                logger.debug('notification already configured on bucket {0}'.format(bucket))
                return

    notif_config = {'LambdaFunctionConfigurations': [lambda_config]}
    logger.info('setting notification on bucket {0}'.format(bucket))
    bn.put(NotificationConfiguration=notif_config)


# TODO remove this if/when CFN adds support for it
def enable_s3_default_encryption(bucket):
    # the current lambda execution environment comes with botocore 1.7.37
    # presumably the next update will include a sufficiently new botocore with this support
    # that's why I'm only checking for this one version
    if botocore.__version__ == '1.7.37':
        logger.warn('botocore version 1.7.37 does not support s3 default encryption')
        return

    # I think catching the exception is the only way to check if default encryption is not enabled
    try:
        resp = s3_client.get_bucket_encryption(Bucket=bucket)
    except botocore.exceptions.ClientError as e:
        if e.response['Error'].get('Code') != 'ServerSideEncryptionConfigurationNotFoundError':
            raise
    else:
        logger.debug('default encryption already enabled on bucket {0}'.format(bucket))
        return

    logger.info('enabling default encryption on bucket {0}'.format(bucket))
    s3_client.put_bucket_encryption(
        Bucket=bucket,
        ServerSideEncryptionConfiguration={
            'Rules': [
                {
                    'ApplyServerSideEncryptionByDefault': {
                        'SSEAlgorithm': 'AES256'
                    }
                },
            ]
        })


def sendResponse(event, context, responseStatus, responseData):
    responseBody = json.dumps({
        'Status': responseStatus,
        'Reason': 'See the details in CloudWatch Log Stream: ' + context.log_stream_name,
        'PhysicalResourceId': context.log_group_name,
        'StackId': event['StackId'],
        'RequestId': event['RequestId'],
        'LogicalResourceId': event['LogicalResourceId'],
        'Data': responseData
    })

    logger.debug('RESPONSE BODY:\n{0}'.format(responseBody))

    if 'ResponseURL' in event:
        url = urlparse(event['ResponseURL'])
        https = http.client.HTTPSConnection(url.hostname)
        https.request('PUT', '{0}?{1}'.format(url.path, url.query), responseBody)
        response = https.getresponse()
        resp = {
            'Status': response.status,
            'Reason': response.reason,
            'Body': response.read().decode(),
            'Headers': dict(response.getheaders())
        }
        logger.debug('S3 PUT RESPONSE:\n{0}'.format(json.dumps(resp)))
