import json
import logging
import os

import boto3
import botocore
import botocore.exceptions

logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.DEBUG)

cfn = boto3.resource('cloudformation')
s3 = boto3.resource('s3')

stack_arn = os.environ['STACK_ARN']
stack = cfn.Stack(stack_arn)

global_artifacts_bucket = None
global_config_bucket = None
regions = None


def lambda_handler(event, context):
    global global_artifacts_bucket, global_config_bucket
    logger.debug('REQUEST:\n{0}'.format(json.dumps(event)))

    global_artifacts_bucket = get_stack_param('Artifacts')
    global_config_bucket = get_stack_output('VPCMConfigBucket')
    regions = get_regions()

    scan_vpcs_zip = s3.Object(global_artifacts_bucket, 'vpcm_scan_vpcs.zip')
    manage_routes_zip = s3.Object(global_artifacts_bucket, get_stack_param('VPCMManageRoutesKey'))
    user_data = s3.Object(global_artifacts_bucket, 'user_data.sh')

    template = s3.Object(global_artifacts_bucket, 'vpcm_cfn_regional-template.yaml').get()['Body'].read().decode()

    stack_name, junk = stack_arn.rsplit('/', 2)[1:]

    for region in regions:
        bucket_name = '{0}-vpcm-artifacts-{1}-{2}'.format(stack_name, region, junk)
        bucket_name = '{0:.63}'.format(bucket_name).strip('-')
        logger.debug(bucket_name)

        scan_vpcs_regional = get_regional_copy(scan_vpcs_zip, bucket_name, region)
        manage_routes_regional = get_regional_copy(manage_routes_zip, bucket_name, region)
        user_data_regional = get_regional_copy(user_data, bucket_name, region)

        params = {
            'instance_profile': get_stack_output('VPCMNodeProfile'),
            'instance_type': get_stack_param('NodeInstanceType'),
            'regional_bucket': bucket_name,
            'vpcm_manage_routes_key': manage_routes_regional.key,
            'vpcm_manage_routes_role': get_stack_output('VPCMManageRoutesRole'),
            'vpcm_scan_vpcs_key': scan_vpcs_regional.key,
            'vpcm_scan_vpcs_role': get_stack_output('VPCMScanVPCsRole'),
            'user_data_key': user_data_regional.key
        }

        tb = template.format(**params)
        logger.debug('created cfn template:\n{0}'.format(tb))

        cfnc = boto3.client('cloudformation', region_name=region)

        try:
            resp = cfnc.get_template(StackName='{0}-regional'.format(stack_name))
        except botocore.exceptions.ClientError as e:
            if e.response['Error'].get('Code') != 'ValidationError':
                raise
            logger.info('Regional stack does not exist in {0}. Creating it.'.format(region))
            resp = cfnc.create_stack(
                StackName='{0}-regional'.format(stack_name), TemplateBody=tb, Capabilities=['CAPABILITY_IAM'])
            logger.debug('create_stack returned:\n{0}'.format(json.dumps(resp, indent=2)))
            continue

        old = resp['TemplateBody']
        if old == tb:
            logger.debug('Template for {0} is unchanged.'.format(region))
            continue

        logger.info('Template for {0} has changed. Updating it.'.format(region))

        resp = cfnc.update_stack(
            StackName='{0}-regional'.format(stack_name), TemplateBody=tb, Capabilities=['CAPABILITY_IAM'])
        logger.debug('update_stack returned:\n{0}'.format(json.dumps(resp, indent=2)))


def copy_object(src, dest, dest_region):
    copy_source = {'Bucket': src.bucket_name, 'Key': src.key}

    try:
        resp = dest.copy_from(CopySource=copy_source)
    except botocore.exceptions.ClientError as e:
        if e.response['Error'].get('Code') != 'NoSuchBucket':
            raise
        logger.debug('bucket {0} not found'.format(dest.bucket_name))
        create_bucket(dest.bucket_name, dest_region)
        resp = dest.copy_from(CopySource=copy_source)


def create_bucket(bucket_name, region):
    rs3 = boto3.resource('s3', region_name=region)
    bucket = rs3.Bucket(bucket_name)
    logger.debug('creating {0} in region {1}'.format(bucket, region))
    resp = bucket.create(CreateBucketConfiguration={'LocationConstraint': region})
    logger.debug('bucket.create returned:\n{0}'.format(resp))


def get_regional_copy(src, bucket_name, region):
    rs3 = boto3.resource('s3', region_name=region)
    obj = rs3.Object(bucket_name, src.e_tag.strip('"'))
    try:
        obj.load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error'].get('Code') != '404':
            raise
        logger.debug('copy of {0} not found in region {1}'.format(src.key, region))
        copy_object(src, obj, region)
    return obj


def get_regions():
    # accept commas, semicolons, and whitespace as valid delimiters
    regions = get_stack_param('Regions').replace(',', ' ').replace(';', ' ').split()

    if len(regions) == 0:
        logger.info('Regions parameter not set. Using all regions!')
        client = boto3.client('ec2')
        resp = client.describe_regions()
        regions = [x['RegionName'] for x in resp['Regions']]

    return regions


def get_stack_output(key):
    try:
        return [x['OutputValue'] for x in stack.outputs if x['OutputKey'] == key][0]
    except IndexError as e:
        return None


def get_stack_param(key):
    try:
        return [x['ParameterValue'] for x in stack.parameters if x['ParameterKey'] == key][0]
    except IndexError as e:
        return None
