import json
import logging
import os

import botocore
import botocore.session

logging.getLogger('botocore').setLevel(logging.CRITICAL)
logger = logging.getLogger()
try:
  logger.setLevel(os.environ['LOG_LEVEL'])
except:
  logger.setLevel(logging.CRITICAL)

PREFIX = 'nodes/'

try:
  PREFIX = os.environ['PREFIX']
except KeyError:
  pass

strip = len(PREFIX)

session = botocore.session.get_session()
client = session.create_client('s3')

def lambda_handler(event, context):
  logger.debug('REQUEST:\n{0}'.format(json.dumps(event)))

  for record in event['Records']:
    process(record)

def process(record):
  if not record['s3']['object']['key'].startswith(PREFIX):
    return

  bucket = record['s3']['bucket']['name']

  resp = client.list_objects_v2(Bucket=bucket, Prefix=PREFIX)
  logger.debug('client.list_objects_v2 returned:\n{0}'.format(resp))

  if resp['KeyCount'] > 0:
    s = ''.join([obj['Key'][strip:] + '\n' for obj in resp['Contents']])
  else:
    s = '\n'

  logger.debug('writing the following to nodes.txt:\n{0}'.format(s))

  resp = client.put_object(Body=s, Bucket=bucket, Key='nodes.txt')

  # TODO: error handling
  #if resp['ResponseMetadata']['HTTPStatusCode'] != 200:
  #  pass
