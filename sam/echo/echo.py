import json
import logging

logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.DEBUG)

def lambda_handler(event, context):
    logger.debug(json.dumps(event, indent=2))
    return json.dumps(event, indent=2)
