import json
import logging

logger = logging.getLogger()
try:
    logger.setLevel(os.environ['LOG_LEVEL'])
except:
    logger.setLevel(logging.DEBUG)


def lambda_handler(event, context):
    logger.debug('REQUEST:\n{0}'.format(json.dumps(event, indent=2)))
