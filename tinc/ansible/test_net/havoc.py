#!/usr/bin/env python2.7
from __future__ import division, print_function
from collections import namedtuple
from pprint import pprint

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import boto3
import boto3.session
import random
import sys

try:
    percentage = int(sys.argv[1])
except:
    percentage = 20

protos = {'all': '-1', 'tcp': '6', 'udp': '17'}
Region = namedtuple('Region', ['name', 'client', 'resource', 'ipv6', 'nacl', 'vpc', 'new_entries', 'new'])
NetworkAclEntry = namedtuple('NetworkAclEntry', ['direction', 'region', 'protocol', 'action', 'from_port', 'to_port'])

with open('group_vars/all.yml', 'rb') as f:
    config = load(f, Loader=Loader)

def make_client_and_resource(region):
    if 'aws_profile' in region:
        session = boto3.session.Session(region_name=region['name'], profile_name=region['aws_profile'])
        return (session.client('ec2'), session.resource('ec2'))
    return (boto3.client('ec2', region_name=region['name']), boto3.resource('ec2', region_name=region['name']))

def make_region(region):
    client, resource = make_client_and_resource(region)
    resp = client.describe_vpcs(Filters=[{'Name': 'tag:Name','Values': [config['vpc_name']]}])
    vpc = resource.Vpc(resp['Vpcs'][0]['VpcId'])

    for i in vpc.instances.all():
        for tag in i.tags:
            if tag['Key'] == 'Name' and tag['Value'] == config['ec2_instance_name']:
                instance = i
                break
        else:
            continue
        break
    else:
        print('oops, no instance found in {0}.'.format(region['name']))
        return None

    for n in vpc.network_acls.all():
        for assoc in n.associations:
            if assoc['SubnetId'] == instance.subnet_id:
                nacl = n
                break
        else:
            continue
        break
    else:
        print('oops, no matching nacl found in {0}.'.format(reg.name))
        return None

    ipv6 = instance.network_interfaces[0].ipv6_addresses[0]['Ipv6Address']
    return Region(region['name'], client, resource, ipv6, nacl, vpc, [], [])

def clear_nacl_entries(nacl):
    my_entries = filter(lambda x: x['RuleNumber'] < 100, nacl.entries)
    map(lambda x: nacl.delete_entry(Egress=x['Egress'], RuleNumber=x['RuleNumber']), my_entries)

regions = filter(None, [make_region(region) for region in config['regions']])

def lookup_ipv6(region):
    for reg in regions:
        if reg.name == region:
            return reg.ipv6

for reg in regions:
    other_regions = filter(lambda x: x != reg, regions)
    to_block = random.sample(other_regions, int(round(len(other_regions) * (percentage/100))))

    for r in to_block:
        reg.new.append(NetworkAclEntry(region=r.name, direction='in', protocol='all', action='deny', from_port=655, to_port=655))
        #for proto in ['tcp', 'udp']:
            #reg.new.append(NetworkAclEntry(region=r.name, direction='in', protocol=proto, action='deny', from_port=655, to_port=655))
            # 32768 - 61000 are default linux ephemeral ports
            #reg.new.append(NetworkAclEntry(region=r.name, direction='in', protocol=proto, action='deny', from_port=32768, to_port=61000))
            #reg.new.append(NetworkAclEntry(region=r.name, direction='out', protocol=proto, action='deny', from_port=32768, to_port=61000))

header = NetworkAclEntry(direction='flow', region='region', protocol='proto', action='action', from_port='from', to_port='to')
fmt = '  {ne.direction:5} {ne.region:14} {ne.protocol:6} {ne.action:7} {ne.from_port:<6} {ne.to_port:<6}'
for reg in regions:
    print('{0}:'.format(reg.name))
    clear_nacl_entries(reg.nacl)
    print(fmt.format(ne=header))
    for i, ne in enumerate(reg.new):
        print(fmt.format(ne=ne))
        e = {'Egress': ne.direction == 'out',
                'Ipv6CidrBlock': '{0}/128'.format(lookup_ipv6(ne.region)),
                'PortRange': {'From': ne.from_port, 'To': ne.to_port},
                'Protocol': protos[ne.protocol],
                'RuleAction': ne.action,
                'RuleNumber': 50+i}
        reg.nacl.create_entry(**e)
    print()
