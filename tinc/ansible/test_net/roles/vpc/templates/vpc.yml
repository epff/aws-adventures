---
Parameters:

  PrivateSubnetAZ:
    Description: Availability zone to use for private subnet
    Type: String

  PrivateSubnetCidrBlock:
    Description: CIDR block to use for private subnet
    Type: String

  PublicSubnetAZ:
    Description: Availability zone to use for public subnet
    Type: String

  PublicSubnetCidrBlock:
    Description: CIDR block to use for public subnet
    Type: String

  VPCCidrBlock:
    Description: CIDR block to use for VPC
    Type: String

  VPCName:
    Description: VPC Name
    Type: String

  SecurityGroupName:
    Description: Name tag for the tinc node security group
    Type: String

  AltHttpPort:
    Description: Alternate port for HTTP
    Type: String

Resources:

  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VPCCidrBlock
      EnableDnsSupport: true
      EnableDnsHostnames: true
      InstanceTenancy: default
      Tags:
        - Key: Name
          Value: !Sub ${VPCName}
        - Key: "vpc_mesh:connect"
          Value: true
        - Key: "vpc_mesh:include_subnets"
          Value: all

  VpcCidrBlockIpv6:
    Type: AWS::EC2::VPCCidrBlock
    Properties:
      VpcId: !Ref VPC
      AmazonProvidedIpv6CidrBlock: true

  InternetGateway:
    Type: AWS::EC2::InternetGateway

  VPCGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
        VpcId: !Ref VPC
        InternetGatewayId: !Ref InternetGateway

  PublicSubnet:
    DependsOn: VpcCidrBlockIpv6
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Ref PublicSubnetAZ
      CidrBlock: !Ref PublicSubnetCidrBlock
      Ipv6CidrBlock: !Join ['', [!Select [0, !Split ['00::/56', !Select [0, !GetAtt 'VPC.Ipv6CidrBlocks']]], '00::/64']]
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub ${VPCName} public
        - Key: vpc_mesh:use_for_transit
          Value: true

  PrivateSubnet:
    DependsOn: VpcCidrBlockIpv6
    Type: AWS::EC2::Subnet
    Properties:
      AssignIpv6AddressOnCreation: true
      AvailabilityZone: !Ref PrivateSubnetAZ
      CidrBlock: !Ref PrivateSubnetCidrBlock
      Ipv6CidrBlock: !Join ['', [!Select [0, !Split ['00::/56', !Select [0, !GetAtt 'VPC.Ipv6CidrBlocks']]], '01::/64']]
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub ${VPCName} private

  RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub ${VPCName} public

  InternetRoute:
    Type: AWS::EC2::Route
    DependsOn: InternetGateway
    Properties:
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
      RouteTableId: !Ref RouteTable

  InternetRouteIpv6:
    Type: AWS::EC2::Route
    DependsOn: InternetGateway
    Properties:
      DestinationIpv6CidrBlock: ::/0
      GatewayId: !Ref InternetGateway
      RouteTableId: !Ref RouteTable

  PublicSubnetRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref RouteTable
      SubnetId: !Ref PublicSubnet

  SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: ICMP, SSH, tinc from anywhere, HTTP from ipv6 anywhere
      SecurityGroupIngress:
        - CidrIp: 0.0.0.0/0
          FromPort: -1
          IpProtocol: icmp
          ToPort: -1
        - CidrIpv6: ::/0
          FromPort: -1
          IpProtocol: 58
          ToPort: -1
        - CidrIp: 0.0.0.0/0
          FromPort: 22
          IpProtocol: tcp
          ToPort: 22
        - CidrIpv6: ::/0
          FromPort: 22
          IpProtocol: tcp
          ToPort: 22
        - CidrIp: 0.0.0.0/0
          FromPort: 655
          IpProtocol: tcp
          ToPort: 655
        - CidrIp: 0.0.0.0/0
          FromPort: 655
          IpProtocol: udp
          ToPort: 655
        - CidrIpv6: ::/0
          FromPort: 655
          IpProtocol: tcp
          ToPort: 655
        - CidrIpv6: ::/0
          FromPort: 655
          IpProtocol: udp
          ToPort: 655
        - CidrIp: 0.0.0.0/0
          FromPort: !Ref AltHttpPort
          IpProtocol: tcp
          ToPort: !Ref AltHttpPort
        - CidrIpv6: ::/0
          FromPort: !Ref AltHttpPort
          IpProtocol: tcp
          ToPort: !Ref AltHttpPort
        - CidrIpv6: ::/0
          FromPort: 80
          IpProtocol: tcp
          ToPort: 80
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Ref SecurityGroupName
