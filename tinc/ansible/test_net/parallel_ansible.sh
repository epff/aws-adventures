#!/usr/bin/env bash

export ANSIBLE_FORCE_COLOR=true
yq -r ".regions[].name" < group_vars/all.yml | parallel -j 0 ansible-playbook site.yml -e region={} "$*"
